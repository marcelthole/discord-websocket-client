# PHP Discord Bot

## First Run
Use this example Boilerplate to start the cli server
```php
<?php

include 'vendor/autoload.php';

$loop = \React\EventLoop\Factory::create();

$pdoStorage = new \DiscordWebsocketClient\Storage\DatabaseAdapter(
    new \PDO('sqlite:data/db.sqlite')
);

$discordConfig = new \DiscordWebsocketClient\DiscordConfig(
    '<TOKEN>',
    $pdoStorage,
    null
);

$messageListener = new \DiscordWebsocketClient\Queue\MessageListener();
$messageListener->add(...[
    \DiscordWebsocketClient\MessageHandler\Heartbeat::createFromConfig($discordConfig),
    \DiscordWebsocketClient\MessageHandler\Identifying::createFromConfig($discordConfig),
    \DiscordWebsocketClient\MessageHandler\PerfomanceLogger::createFromConfig($discordConfig),
    \DiscordWebsocketClient\MessageHandler\MemberUpdater::createFromConfig($discordConfig),
]);
$gateway = new \DiscordWebsocketClient\DiscordGateway(
    $loop,
    $discordConfig,
    $messageListener
);
$gateway->listen();
$loop->run();
```

# Example Messages

- Receive Channel ID with `#channel-name`
```json
{
	"t": "MESSAGE_CREATE",
	"s": 1,
	"op": 0,
	"d": {
		"content": "<#610887927416712345>"
	}
}
```

- Receive User ID with `@user`
```json
{
	"t": "MESSAGE_CREATE",
	"s": 1,
	"op": 0,
	"d": {
		"content": "<@610612445966412345>"
	}
}
```
