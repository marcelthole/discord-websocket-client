<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Queue;

use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\MessageHandler\MessageHandler;
use Ratchet\Client\WebSocket;
use React\EventLoop\LoopInterface;
use function assert;

class MessageListener
{
    /** @var array<int, MessageHandler> */
    private $handler;

    public function add(MessageHandler ...$handlers) : void
    {
        foreach ($handlers as $messageHandler) {
            $this->handler[] = $messageHandler;
        }
    }

    /**
     * @return array<int, MessageHandler>
     */
    public function getRegisteredHandlers() : array
    {
        return $this->handler;
    }

    public function clear() : void
    {
        $this->handler = [];
    }

    public function handleMesssageEvents(
        LoopInterface $loop,
        WebSocket $webSocket,
        Payload $payload
    ) : void {
        foreach ($this->handler as $handler) {
            assert($handler instanceof MessageHandler);
            $handler->handleMessage($loop, $webSocket, $payload);
        }
    }

    public function handleClose(LoopInterface $loop, WebSocket $webSocket) : void
    {
        foreach ($this->handler as $handler) {
            assert($handler instanceof MessageHandler);
            $handler->handleClose($loop, $webSocket);
        }
    }
}
