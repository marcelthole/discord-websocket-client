<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Storage;

use PDO;
use PDOStatement;

class DatabaseAdapter implements StorageAdapter
{
    /** @var PDO */
    private $pdo;

    public function __construct(PDO $pdoConnection)
    {
        $this->pdo = $pdoConnection;
        $this->pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        $this->pdo->setAttribute(PDO::ATTR_DEFAULT_FETCH_MODE, PDO::FETCH_OBJ);
    }

    /** @param mixed[] $args */
    public function query(string $sql, array $args = []) : PDOStatement
    {
        $stmt = $this->pdo->prepare($sql);
        foreach ($args as $name => $value) {
            $stmt->bindValue(':' . $name, $value);
        }
        $stmt->execute();

        return $stmt;
    }
}
