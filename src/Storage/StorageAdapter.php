<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Storage;

use PDOStatement;

interface StorageAdapter
{
    /** @param mixed[] $args */
    public function query(string $sql, array $args = []) : PDOStatement;
}
