<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Storage\Entity;

use DateTimeImmutable;

class Member
{
    /** @var int */
    private $id;
    /** @var string */
    private $username;
    /** @var string|null */
    private $nick;
    /** @var DateTimeImmutable */
    private $updatedAt;

    public function __construct(int $id, string $username, ?string $nick)
    {
        $this->id        = $id;
        $this->username  = $username;
        $this->nick      = $nick;
        $this->updatedAt = new DateTimeImmutable();
    }

    public function getId() : int
    {
        return $this->id;
    }

    public function setId(int $id) : self
    {
        $this->id = $id;

        return $this;
    }

    public function getUsername() : string
    {
        return $this->username;
    }

    public function setUsername(string $username) : self
    {
        $this->username = $username;

        return $this;
    }

    public function getNick() : ?string
    {
        return $this->nick;
    }

    public function setNick(?string $nick) : self
    {
        $this->nick = $nick;

        return $this;
    }

    public function getUpdatedAt() : DateTimeImmutable
    {
        return $this->updatedAt;
    }

    public function setUpdatedAt(DateTimeImmutable $updatedAt) : self
    {
        $this->updatedAt = $updatedAt;

        return $this;
    }
}
