<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Storage\Repository;

use DiscordWebsocketClient\Storage\StorageAdapter;
use PDO;

class MessageReactionsRepository
{
    /** @var StorageAdapter */
    private $storageAdapter;

    public function __construct(StorageAdapter $storageAdapter)
    {
        $this->storageAdapter = $storageAdapter;
    }

    public function addReaction(int $messageId, int $userId, string $reaction) : void
    {
        $this->storageAdapter->query(
            'INSERT INTO MessageReactions (messageId, userId, reaction, createdAt) VALUES (:messageId, :userId, :reaction, CURRENT_TIMESTAMP)',
            [
                'messageId' => $messageId,
                'userId'    => $userId,
                'reaction'  => $reaction,
            ]
        );
    }

    public function removeReaction(int $messageId, int $userId, string $reaction) : void
    {
        $this->storageAdapter->query(
            'DELETE FROM MessageReactions WHERE messageId = :messageId AND userId = :userId AND reaction = :reaction',
            [
                'messageId' => $messageId,
                'userId'    => $userId,
                'reaction'  => $reaction,
            ]
        );
    }

    /**
     * @return array<string, array<int, string>>
     */
    public function getMessageReactions(int $messageId) : array
    {
        $stmt = $this->storageAdapter->query(
            'SELECT mr.reaction, m.username, m.nick FROM MessageReactions AS mr LEFT JOIN Members AS m ON mr.userId = m.id WHERE mr.messageId = :messageId',
            ['messageId' => $messageId]
        );

        $allReaction = $stmt->fetchAll(PDO::FETCH_OBJ) ?: [];
        $reactions   = [];
        foreach ($allReaction as $reaction) {
            $emoji = (string) $reaction->reaction;
            if (! isset($reactions[$emoji])) {
                $reactions[$emoji] = [];
            }
            $reactions[$emoji][] = (string) ($reaction->nick ?: $reaction->username);
        }

        return $reactions;
    }
}
