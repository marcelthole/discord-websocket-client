<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Storage\Repository;

use DateTimeImmutable;
use DiscordWebsocketClient\Storage\Entity\Member;
use DiscordWebsocketClient\Storage\StorageAdapter;
use PDO;

class MemberRepository
{
    /** @var StorageAdapter */
    private $storageAdapter;

    public function __construct(StorageAdapter $storageAdapter)
    {
        $this->storageAdapter = $storageAdapter;
    }

    public function getMemberById(int $memberId) : ?Member
    {
        $query = $this->storageAdapter->query(
            'SELECT * FROM Members WHERE id = :id',
            ['id' => $memberId]
        );
        $row   = $query->fetch(PDO::FETCH_OBJ);
        if (empty($row)) {
            return null;
        }
        $member = new Member(
            (int) $row->id,
            $row->username,
            $row->nick
        );
        $member->setUpdatedAt(new DateTimeImmutable($row->updatedAt));

        return $member;
    }

    public function createOrUpdateMember(Member $member) : void
    {
        $currentMember = $this->getMemberById($member->getId());
        if ($currentMember === null) {
            $this->storageAdapter->query(
                'INSERT INTO Members (id, username, nick, updatedAt) VALUES (:id, :username, :nick, CURRENT_TIMESTAMP)',
                [
                    'id'       => $member->getId(),
                    'username' => $member->getUsername(),
                    'nick'     => $member->getNick(),
                ]
            );
        } else {
            $this->storageAdapter->query(
                'UPDATE Members SET username = :username, nick = :nick, updatedAt = CURRENT_TIMESTAMP WHERE id = :id',
                [
                    'username' => $member->getUsername(),
                    'nick'     => $member->getNick(),
                    'id'       => $member->getId(),
                ]
            );
        }
    }
}
