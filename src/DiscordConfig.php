<?php

declare(strict_types=1);

namespace DiscordWebsocketClient;

use DiscordWebsocketClient\Storage\StorageAdapter;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;

class DiscordConfig
{
    private const WSS_GATEWAY = 'wss://gateway.discord.gg/?v=6&encoding=json';

    /** @var LoggerInterface */
    private $logger;
    /** @var string */
    private $websocketUrl;
    /** @var string */
    private $token;
    /** @var StorageAdapter */
    private $storageAdapter;

    public function __construct(
        string $token,
        StorageAdapter $storageAdapter,
        ?LoggerInterface $logger = null
    ) {
        if ($logger === null) {
            $logger = new NullLogger();
        }
        $this->setWebsocketUrl(self::WSS_GATEWAY);
        $this->storageAdapter = $storageAdapter;
        $this->token          = $token;
        $this->logger         = $logger;
    }

    public function getWebsocketUrl() : string
    {
        return $this->websocketUrl;
    }

    public function getStorageAdapter() : StorageAdapter
    {
        return $this->storageAdapter;
    }

    public function setWebsocketUrl(string $websocketUrl) : void
    {
        $this->websocketUrl = $websocketUrl;
    }

    public function getLogger() : LoggerInterface
    {
        return $this->logger;
    }

    public function setLogger(LoggerInterface $logger) : void
    {
        $this->logger = $logger;
    }

    public function getToken() : string
    {
        return $this->token;
    }
}
