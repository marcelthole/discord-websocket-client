<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Gateway;

use JsonException;
use JsonSerializable;
use stdClass;
use const JSON_THROW_ON_ERROR;
use function is_object;
use function json_decode;
use function property_exists;

final class Payload implements JsonSerializable
{
    /** dispatches an event */
    public const OP_DISPATCH = 0;
    /** used for ping checking */
    public const OP_HEARTBEAT = 1;
    /** used for client handshake */
    public const OP_IDENTIFY = 2;
    /** used to update the client status */
    public const OP_STATUS_UPDATE = 3;
    /** used to join/move/leave voice channels */
    public const OP_VOICE_STATE_UPDATE = 4;
    /** used to resume a closed connection */
    public const OP_RESUME = 6;
    /** used to tell clients to reconnect to the gateway */
    public const OP_RECONNECT = 7;
    /** used to request guild members */
    public const OP_REQUEST_GUILD_MEMBERS = 8;
    /** used to notify client they have an invalid session id */
    public const OP_INVALID_SESSION = 9;
    /** sent immediately after connecting, contains heartbeat and server debug information */
    public const OP_HELLO = 10;
    /** sent immediately following a client heartbeat that was received */
    public const OP_HEARTBEAT_ACK = 11;

    /**
     * opcode for the payload
     *
     * @var int
     */
    private $op;
    /**
     * any JSON value event data
     *
     * @var stdClass|null
     */
    private $d;
    /**
     * sequence number, used for resuming sessions and heartbeats
     *
     * @var int|null
     */
    private $s;
    /**
     * the event name for this payload
     *
     * @var string|null
     */
    private $t;

    public function __construct(int $operation, ?stdClass $data, ?int $sequenceNumber, ?string $event)
    {
        $this->op = $operation;
        $this->d  = $data;
        $this->s  = $sequenceNumber;
        $this->t  = $event;
    }

    public static function createFromResponse(string $jsonResponse) : self
    {
        $decoded = json_decode($jsonResponse, false, 512, JSON_THROW_ON_ERROR);

        if (! is_object($decoded)) {
            throw new JsonException('Expected decode object!', 1565626971711);
        }

        if (! property_exists($decoded, 'd')
            || ! property_exists($decoded, 'op')
            || ! property_exists($decoded, 's')
            || ! property_exists($decoded, 't')
        ) {
            throw new JsonException('Invalid object given', 1565626751194);
        }

        return new self($decoded->op, $decoded->d ?: null, $decoded->s, $decoded->t);
    }

    public function getOperationCode() : int
    {
        return $this->op;
    }

    public function getData() : ?stdClass
    {
        return $this->d;
    }

    public function getSequenceNumber() : ?int
    {
        return $this->s;
    }

    public function getEventName() : ?string
    {
        return $this->t;
    }

    /**
     * @return array<string, int|stdClass|string|null>
     */
    public function jsonSerialize() : array
    {
        return [
            'op' => $this->op,
            'd'  => $this->d,
            's'  => $this->s,
            't'  => $this->t,
        ];
    }

    public function isHello() : bool
    {
        return $this->op === self::OP_HELLO;
    }

    public function isHeartbeatAck() : bool
    {
        return $this->op === self::OP_HEARTBEAT_ACK;
    }

    public function isInvalidSessionToken() : bool
    {
        return $this->op === self::OP_INVALID_SESSION;
    }

    public function isDispatchEvent() : bool
    {
        return $this->op === self::OP_DISPATCH;
    }
}
