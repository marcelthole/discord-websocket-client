<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Gateway;

use JsonSerializable;
use stdClass;
use const PHP_OS;

class IdentifyRequest implements JsonSerializable
{
    /** @var string */
    private $token;

    public function __construct(string $token)
    {
        $this->token = $token;
    }

    /**
     * @return array<string,int|stdClass|string|null>
     */
    public function jsonSerialize() : array
    {
        return (new Payload(
            Payload::OP_IDENTIFY,
            (object) [
                'token'      => $this->token,
                'properties' => [
                    '$os'      => PHP_OS,
                    '$browser' => 'Discord PHP Client',
                ],
            ],
            null,
            null
        ))->jsonSerialize();
    }
}
