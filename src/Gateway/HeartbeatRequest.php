<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Gateway;

use JsonSerializable;
use stdClass;

class HeartbeatRequest implements JsonSerializable
{
    /** @var int|null */
    private $sequenceNumber;

    public function __construct(?int $sequenceNumber)
    {
        $this->sequenceNumber = $sequenceNumber;
    }

    /**
     * @return array<string,int|stdClass|string|null>
     */
    public function jsonSerialize() : array
    {
        return (new Payload(
            Payload::OP_HEARTBEAT,
            (object) [],
            $this->sequenceNumber,
            null
        ))->jsonSerialize();
    }
}
