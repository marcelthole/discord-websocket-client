<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Gateway;

use JsonSerializable;
use stdClass;

class ResumingRequest implements JsonSerializable
{
    /** @var string */
    private $sessionId;
    /** @var int */
    private $sequenceNumber;
    /** @var string */
    private $token;

    public function __construct(string $token, string $sessionId, int $sequenceNumber)
    {
        $this->sessionId      = $sessionId;
        $this->sequenceNumber = $sequenceNumber;
        $this->token          = $token;
    }

    /**
     * @return array<string,int|stdClass|string|null>
     */
    public function jsonSerialize() : array
    {
        return (new Payload(
            Payload::OP_RESUME,
            (object) [
                'token'      => $this->token,
                'session_id' => $this->sessionId,
                'seq'        => $this->sequenceNumber,
            ],
            null,
            null
        ))->jsonSerialize();
    }
}
