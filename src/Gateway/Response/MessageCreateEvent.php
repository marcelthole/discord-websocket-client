<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Gateway\Response;

use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\Gateway\Response\DTO\Author;
use InvalidArgumentException;

class MessageCreateEvent
{
    public const EVENT_NAME = 'MESSAGE_CREATE';
    /** @var Author */
    private $author;
    /** @var int */
    private $channelId;
    /** @var int */
    private $messageId;
    /** @var string */
    private $content;

    public function __construct(Author $author, int $channelId, int $messageId, string $content)
    {
        $this->author    = $author;
        $this->channelId = $channelId;
        $this->messageId = $messageId;
        $this->content   = $content;
    }

    public static function createFromPayload(Payload $payload) : self
    {
        $data = $payload->getData();
        if ($data === null) {
            throw new InvalidArgumentException('Got invalid payload data', 1566066663624);
        }
        $author    = Author::createFromResponse($data->author);
        $channelId = (int) $data->channel_id;
        $messageId = (int) $data->id;
        $content   = $data->content;

        return new self($author, $channelId, $messageId, $content);
    }

    public function getAuthor() : Author
    {
        return $this->author;
    }

    public function getChannelId() : int
    {
        return $this->channelId;
    }

    public function getContent() : string
    {
        return $this->content;
    }

    public function getMessageId() : int
    {
        return $this->messageId;
    }
}
