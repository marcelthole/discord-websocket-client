<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Gateway\Response;

use DiscordWebsocketClient\Gateway\Payload;
use InvalidArgumentException;

class ReadyEvent
{
    public const EVENT_NAME = 'READY';

    /** @var string */
    private $sessionId;

    public function __construct(string $sessionId)
    {
        $this->sessionId = $sessionId;
    }

    public static function createFromPayload(Payload $payload) : self
    {
        $data = $payload->getData();
        if ($data === null) {
            throw new InvalidArgumentException('Got invalid payload data', 1566066693052);
        }

        return new self($data->session_id);
    }

    public function getSessionId() : string
    {
        return $this->sessionId;
    }
}
