<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Gateway\Response;

use DiscordWebsocketClient\Gateway\Payload;

class ResumedEvent
{
    public const EVENT_NAME = 'RESUMED';

    public function __construct()
    {
    }

    public static function createFromPayload(Payload $payload) : self
    {
        return new self();
    }
}
