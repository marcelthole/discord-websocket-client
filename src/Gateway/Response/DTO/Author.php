<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Gateway\Response\DTO;

use stdClass;

class Author
{
    /** @var string */
    private $username;
    /** @var string */
    private $id;
    /** @var bool */
    private $isBot;

    public function __construct(string $username, string $id, bool $isBot)
    {
        $this->username = $username;
        $this->id       = $id;
        $this->isBot    = $isBot;
    }

    public static function createFromResponse(stdClass $author) : self
    {
        return new self(
            $author->username,
            $author->id,
            $author->bot ?? false
        );
    }

    public function isBot() : bool
    {
        return $this->isBot;
    }

    public function getId() : string
    {
        return $this->id;
    }

    public function getUsername() : string
    {
        return $this->username;
    }
}
