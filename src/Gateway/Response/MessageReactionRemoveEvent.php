<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Gateway\Response;

use DiscordWebsocketClient\Gateway\Payload;
use InvalidArgumentException;

class MessageReactionRemoveEvent
{
    public const EVENT_NAME = 'MESSAGE_REACTION_REMOVE';
    /** @var int */
    private $userId;
    /** @var int */
    private $messageId;
    /** @var int */
    private $channelId;
    /** @var string */
    private $emoji;

    public function __construct(int $userId, int $messageId, int $channelId, string $emoji)
    {
        $this->userId    = $userId;
        $this->messageId = $messageId;
        $this->channelId = $channelId;
        $this->emoji     = $emoji;
    }

    public static function createFromPayload(Payload $payload) : self
    {
        $data = $payload->getData();
        if ($data === null) {
            throw new InvalidArgumentException('Got invalid payload data', 1566066687040);
        }
        $userId    = (int) $data->user_id;
        $messageId = (int) $data->message_id;
        $channelId = (int) $data->channel_id;
        $emoji     = $data->emoji->name;

        return new self($userId, $messageId, $channelId, $emoji);
    }

    public function getMessageId() : int
    {
        return $this->messageId;
    }

    public function getChannelId() : int
    {
        return $this->channelId;
    }

    public function getEmoji() : string
    {
        return $this->emoji;
    }

    public function getUserId() : int
    {
        return $this->userId;
    }
}
