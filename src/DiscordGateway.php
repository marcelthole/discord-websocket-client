<?php

declare(strict_types=1);

namespace DiscordWebsocketClient;

use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\Queue\MessageListener;
use DiscordWebsocketClient\Types\WebSocketEventType;
use DiscordWebsocketClient\Types\WsCloseStatusCodes;
use Ratchet\Client\Connector;
use Ratchet\Client\WebSocket;
use Ratchet\RFC6455\Messaging\Message;
use React\EventLoop\LoopInterface;
use React\Promise\PromiseInterface;
use Throwable;
use function assert;
use function sprintf;

class DiscordGateway
{
    /** @var DiscordConfig */
    private $config;
    /** @var LoopInterface */
    private $loop;
    /** @var Connector */
    private $connector;
    /** @var MessageListener */
    private $messageListener;

    public function __construct(
        LoopInterface $loop,
        DiscordConfig $config,
        MessageListener $messageListener,
        ?Connector $connector = null
    ) {
        $this->loop            = $loop;
        $this->config          = $config;
        $this->messageListener = $messageListener;

        if ($connector === null) {
            $connector = new Connector($this->loop, null);
        }
        $this->connector = $connector;
    }

    public function listen() : PromiseInterface
    {
        $promise = ($this->connector)($this->config->getWebsocketUrl(), [], []);
        assert($promise instanceof PromiseInterface);

        $promise->then(
            function (WebSocket $webSocket) : void {
                $this->config->getLogger()->debug('Connected to websocket ' . $this->config->getWebsocketUrl());

                $webSocket->on(WebSocketEventType::TYPE_MESSAGE, function (Message $msg) use ($webSocket) : void {
                    $this->config->getLogger()->debug('Received message: ' . $msg->getPayload());
                    $payload = Payload::createFromResponse($msg->getPayload());

                    $this->messageListener->handleMesssageEvents($this->loop, $webSocket, $payload);
                });

                $webSocket->on(WebSocketEventType::TYPE_CLOSE, function (?int $code, ?string $reason) use ($webSocket) : void {
                    $this->config->getLogger()->warning(sprintf(
                        'Received close WS Event (Code: %d, Reason: %s)',
                        $code,
                        $reason
                    ));
                    $this->messageListener->handleClose($this->loop, $webSocket);
                    if ($code === WsCloseStatusCodes::INVALID_TOKEN) {
                        return;
                    }

                    $this->config->getLogger()->info('Restart WebSocket');
                    $webSocket->removeAllListeners();

                    $this->listen();
                });
            },
            function (Throwable $exception) : void {
                $this->config->getLogger()->critical(sprintf('Could not connect: %s', $exception->getMessage()), ['exception' => $exception]);
                $this->loop->stop();
            }
        );

        return $promise;
    }
}
