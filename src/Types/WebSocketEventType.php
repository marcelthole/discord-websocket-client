<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Types;

class WebSocketEventType
{
    public const TYPE_CLOSE   = 'close';
    public const TYPE_MESSAGE = 'message';
}
