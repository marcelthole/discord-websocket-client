<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Types;

class WsCloseStatusCodes
{
    public const HEARTBEAT_ACK_MISMATCH = 4300;
    public const INVALID_TOKEN          = 4301;
}
