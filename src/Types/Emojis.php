<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\Types;

class Emojis
{
    public const THUMBS_UP       = "\u{1F44D}";
    public const SHRUG           = "\u{1F937}";
    public const KEY_CAP_DIGIT_0 = "\u{0030}\u{20E3}";
    public const KEY_CAP_DIGIT_1 = "\u{0031}\u{20E3}";
    public const KEY_CAP_DIGIT_2 = "\u{0032}\u{20E3}";
    public const KEY_CAP_DIGIT_3 = "\u{0033}\u{20E3}";
    public const KEY_CAP_DIGIT_4 = "\u{0034}\u{20E3}";
    public const KEY_CAP_DIGIT_5 = "\u{0035}\u{20E3}";
    public const KEY_CAP_DIGIT_6 = "\u{0036}\u{20E3}";
    public const KEY_CAP_DIGIT_7 = "\u{0037}\u{20E3}";
    public const KEY_CAP_DIGIT_8 = "\u{0038}\u{20E3}";
    public const KEY_CAP_DIGIT_9 = "\u{0039}\u{20E3}";
}
