<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\MessageHandler;

use DiscordWebsocketClient\DiscordConfig;
use DiscordWebsocketClient\Gateway\HeartbeatRequest;
use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\Types\WsCloseStatusCodes;
use Psr\Log\LoggerInterface;
use Ratchet\Client\WebSocket;
use React\EventLoop\LoopInterface;
use React\EventLoop\TimerInterface;
use function hrtime;
use function json_encode;
use function sprintf;

class Heartbeat implements MessageHandler
{
    /** @var LoggerInterface */
    private $logger;
    /** @var int|null */
    private $lastSequenceNumber;
    /** @var int */
    private $heartbeatSendAt = 0;
    /** @var int */
    private $heartbeatAckAt = 0;
    /** @var TimerInterface|null */
    private $timer;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function createFromConfig(DiscordConfig $config) : self
    {
        return new self($config->getLogger());
    }

    public function handleMessage(LoopInterface $loop, WebSocket $webSocket, Payload $payload) : void
    {
        $this->lastSequenceNumber = $payload->getSequenceNumber();

        if ($payload->isHello()) {
            $this->heartbeatSendAt = 0;
            $this->heartbeatAckAt  = 0;
            $payloadData           = $payload->getData();
            $interval              = ($payloadData->heartbeat_interval ?? 60000) / 1000;
            $this->logger->debug(sprintf('Received Hello Message with interval of %.2f s', $interval));
            $this->timer = $loop->addPeriodicTimer(
                $interval,
                function () use ($webSocket) : void {
                    if ($this->heartbeatAckAt < $this->heartbeatSendAt) {
                        $this->logger->warning('Heartbeat ACK and Send time mismatch', [
                            'ack'  => $this->heartbeatAckAt,
                            'send' => $this->heartbeatSendAt,
                        ]);
                        // If a client does not receive a heartbeat ack between its attempts at sending heartbeats,
                        // it should immediately terminate the connection with a non-1000 close code,
                        // reconnect, and attempt to resume.
                        $webSocket->close(WsCloseStatusCodes::HEARTBEAT_ACK_MISMATCH, 'heartbeat ACK Mismatch');

                        return;
                    }
                    $this->sendHeartbeatRequest($webSocket);
                }
            );
        }

        if ($payload->isHeartbeatAck()) {
            $this->logger->debug('Received Heartbeat ACK');
            $this->heartbeatAckAt = (int) hrtime(true);

            return;
        }
    }

    public function handleClose(LoopInterface $loop, WebSocket $webSocket) : void
    {
        if ($this->timer === null) {
            return;
        }

        $this->logger->debug('Stop Heartbeat timer');
        $loop->cancelTimer($this->timer);
    }

    private function sendHeartbeatRequest(WebSocket $webSocket) : void
    {
        $this->logger->debug('Send Heartbeat');
        $this->heartbeatSendAt = (int) hrtime(true);
        $webSocket->send(json_encode(new HeartbeatRequest($this->lastSequenceNumber)));
    }
}
