<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\MessageHandler;

use DiscordWebsocketClient\Gateway\Payload;
use Ratchet\Client\WebSocket;
use React\EventLoop\LoopInterface;

interface MessageHandler
{
    public function handleMessage(LoopInterface $loop, WebSocket $webSocket, Payload $payload) : void;

    public function handleClose(LoopInterface $loop, WebSocket $webSocket) : void;
}
