<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\MessageHandler;

use DiscordWebsocketClient\DiscordConfig;
use DiscordWebsocketClient\Gateway\Payload;
use Psr\Log\LoggerInterface;
use Ratchet\Client\WebSocket;
use React\EventLoop\LoopInterface;
use React\EventLoop\TimerInterface;
use function memory_get_peak_usage;
use function memory_get_usage;
use function number_format;

class PerfomanceLogger implements MessageHandler
{
    /** @var LoggerInterface */
    private $logger;
    /** @var TimerInterface|null */
    private $timer;

    public function __construct(LoggerInterface $logger)
    {
        $this->logger = $logger;
    }

    public static function createFromConfig(DiscordConfig $config) : self
    {
        return new self($config->getLogger());
    }

    public function handleMessage(LoopInterface $loop, WebSocket $webSocket, Payload $payload) : void
    {
        if (! $payload->isHello()) {
            return;
        }

        $this->logger->debug('Add Perfomance Logger');
        $this->timer = $loop->addPeriodicTimer(
            600,
            function () : void {
                $this->logger->debug('Perfomance Info', [
                    'memory_peak_mb' => number_format(memory_get_peak_usage(true) / 1024 / 1024, 3, ',', ''),
                    'memory_usage_mb' => number_format(memory_get_usage(true) / 1024 / 1024, 3, ',', ''),
                ]);
            }
        );
    }

    public function handleClose(LoopInterface $loop, WebSocket $webSocket) : void
    {
        if ($this->timer === null) {
            return;
        }

        $this->logger->debug('Stop Perfomance Logger');
        $loop->cancelTimer($this->timer);
    }
}
