<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\MessageHandler;

use DiscordWebsocketClient\DiscordConfig;
use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\Storage\Entity\Member;
use DiscordWebsocketClient\Storage\Repository\MemberRepository;
use Psr\Log\LoggerInterface;
use Ratchet\Client\WebSocket;
use React\EventLoop\LoopInterface;

class MemberUpdater implements MessageHandler
{
    /** @var LoggerInterface */
    private $logger;
    /** @var MemberRepository */
    private $memberRepository;

    public function __construct(
        LoggerInterface $logger,
        MemberRepository $memberRepository
    ) {
        $this->logger           = $logger;
        $this->memberRepository = $memberRepository;
    }

    public static function createFromConfig(DiscordConfig $discordConfig) : self
    {
        return new self(
            $discordConfig->getLogger(),
            new MemberRepository($discordConfig->getStorageAdapter())
        );
    }

    public function handleMessage(LoopInterface $loop, WebSocket $webSocket, Payload $payload) : void
    {
        if ($payload->getEventName() === 'GUILD_CREATE') {
            $members = $payload->getData()->members ?? [];
            foreach ($members as $member) {
                if ($member->user->bot ?? false) {
                    continue;
                }
                $member = new Member(
                    (int) $member->user->id,
                    $member->user->username,
                    $member->nick
                );
                $this->memberRepository->createOrUpdateMember(
                    $member
                );
            }
        }
        if ($payload->getEventName() !== 'GUILD_MEMBER_ADD' && $payload->getEventName() !== 'GUILD_MEMBER_UPDATE') {
            return;
        }

        $member = $payload->getData();
        if ($member === null) {
            return;
        }
        if ($member->user->bot ?? false) {
            return;
        }
        $member = new Member(
            (int) $member->user->id,
            $member->user->username,
            $member->nick
        );
        $this->logger->debug('Update Member ' . $member->getUsername());
        $this->memberRepository->createOrUpdateMember(
            $member
        );
    }

    public function handleClose(LoopInterface $loop, WebSocket $webSocket) : void
    {
        // do nothing
    }
}
