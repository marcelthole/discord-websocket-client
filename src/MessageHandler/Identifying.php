<?php

declare(strict_types=1);

namespace DiscordWebsocketClient\MessageHandler;

use DiscordWebsocketClient\DiscordConfig;
use DiscordWebsocketClient\Gateway\IdentifyRequest;
use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\Gateway\Response\ReadyEvent;
use DiscordWebsocketClient\Gateway\Response\ResumedEvent;
use DiscordWebsocketClient\Gateway\ResumingRequest;
use DiscordWebsocketClient\Types\WsCloseStatusCodes;
use Psr\Log\LoggerInterface;
use Ratchet\Client\WebSocket;
use React\EventLoop\LoopInterface;
use function json_encode;
use function random_int;
use function sleep;

class Identifying implements MessageHandler
{
    /** @var LoggerInterface */
    private $logger;
    /** @var string */
    private $token;
    /** @var int */
    private $lastSequenceNumber = 0;
    /** @var string|null */
    private $sessionId;

    public function __construct(LoggerInterface $logger, string $token)
    {
        $this->logger = $logger;
        $this->token  = $token;
    }

    public static function createFromConfig(DiscordConfig $config) : self
    {
        return new self($config->getLogger(), $config->getToken());
    }

    public function handleMessage(LoopInterface $loop, WebSocket $webSocket, Payload $payload) : void
    {
        if ($payload->getSequenceNumber() !== null) {
            $this->lastSequenceNumber = $payload->getSequenceNumber();
        }

        if ($payload->isHello()) {
            if ($this->sessionId === null) {
                $this->logger->notice('Send Identify Request', [
                    'request' => new IdentifyRequest($this->token),
                ]);
                $webSocket->send(json_encode(new IdentifyRequest($this->token)));
            } else {
                $resumingRequest = new ResumingRequest(
                    $this->token,
                    $this->sessionId,
                    $this->lastSequenceNumber
                );
                $this->logger->notice('Send Resuming Request', ['request' => $resumingRequest]);
                $webSocket->send(json_encode($resumingRequest));
            }
        }

        if ($payload->isInvalidSessionToken()) {
            if ($this->sessionId !== null) {
                $this->sessionId = null;
                sleep(random_int(1, 5));

                $this->logger->info('Send new Identify Request', [
                    'request' => new IdentifyRequest($this->token),
                ]);
                $webSocket->send(json_encode(new IdentifyRequest($this->token)));
            } else {
                $this->logger->warning('The token is not valid', [
                    'token' => $this->token,
                ]);
                $webSocket->close(WsCloseStatusCodes::INVALID_TOKEN, 'token is invalid');
            }
        }

        if ($payload->isDispatchEvent() && $payload->getEventName() === ResumedEvent::EVENT_NAME) {
            $this->logger->notice('Successfully resumed session');
        }

        if (! $payload->isDispatchEvent() || $payload->getEventName() !== ReadyEvent::EVENT_NAME) {
            return;
        }

        $readyEvent = ReadyEvent::createFromPayload($payload);

        $this->logger->debug('Got Ready event with Session info', [
            'sessionId' => $readyEvent->getSessionId(),
        ]);
        $this->sessionId = $readyEvent->getSessionId();
    }

    public function handleClose(LoopInterface $loop, WebSocket $webSocket) : void
    {
        // do nothing
    }
}
