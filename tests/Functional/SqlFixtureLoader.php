<?php


namespace DiscordWebsocketClientTest\Functional;


class SqlFixtureLoader
{

    public static function load(string $file): string
    {
        return file_get_contents('data/sql/' . $file) ?: '';
    }
}
