<?php


namespace DiscordWebsocketClientTest\Functional;


use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\Gateway\Response\ReadyEvent;
use Exception;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use React\EventLoop\LoopInterface;

class TestWebsockteComponent implements MessageComponentInterface
{
    public static $sendHeartbeatAck = true;

    /** @var LoopInterface */
    private $loop;

    /** @var int */
    private $sequenceNumber = 0;

    public function __construct(LoopInterface $loop)
    {
        $this->loop = $loop;
    }

    public function onOpen(ConnectionInterface $conn): void
    {
        $conn->send(json_encode([
            'op' => Payload::OP_HELLO,
            'd'  => ['heartbeat_interval' => 600],
            's'  => null,
            't'  => null,
        ]) ?: '');
    }

    public function onClose(ConnectionInterface $conn): void
    {
        $conn->close();
    }

    public function onError(ConnectionInterface $conn, Exception $e): void
    {
        $conn->close();
        throw $e;
    }

    public function onMessage(ConnectionInterface $from, $msg): void
    {
        $msgObj = json_decode($msg);
        if ($msgObj->op === Payload::OP_HEARTBEAT && self::$sendHeartbeatAck) {
            $from->send(json_encode([
                'op' => Payload::OP_HEARTBEAT_ACK,
                'd'  => [],
                's'  => ++$this->sequenceNumber,
                't'  => null,
            ]) ?: '');
        }

        if ($msgObj->op === Payload::OP_IDENTIFY) {
            $from->send(json_encode([
                'op' => Payload::OP_DISPATCH,
                'd'  => ['session_id' => bin2hex(random_bytes(4))],
                's'  => ++$this->sequenceNumber,
                't'  => ReadyEvent::EVENT_NAME,
            ]) ?: '');
        }
    }
}
