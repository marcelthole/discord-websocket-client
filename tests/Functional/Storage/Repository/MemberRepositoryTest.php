<?php

namespace DiscordWebsocketClientTest\Functional\Storage\Repository;

use DiscordWebsocketClient\Storage\DatabaseAdapter;
use DiscordWebsocketClient\Storage\Entity\Member;
use DiscordWebsocketClient\Storage\Repository\MemberRepository;
use DiscordWebsocketClientTest\Functional\SqlFixtureLoader;
use PHPUnit\Framework\TestCase;

class MemberRepositoryTest extends TestCase
{
    /** @var DatabaseAdapter */
    private $adapter;

    protected function setUp(): void
    {
        $this->adapter = new DatabaseAdapter(new \PDO('sqlite::memory:'));
        $this->adapter->query(SqlFixtureLoader::load('00-Members.sql'));
    }

    public function testCreateOrUpdateMember(): void
    {
        $start = new \DateTimeImmutable();
        $repo = new MemberRepository($this->adapter);
        $member = new Member(1, 'Test User', null);

        $fetchedMemberBefore = $repo->getMemberById(1);
        $repo->createOrUpdateMember($member);
        $fetchedMember = $repo->getMemberById(1);

        self::assertNull($fetchedMemberBefore);
        self::assertNotNull($fetchedMember);
        self::assertSame('Test User', $fetchedMember->getUsername());
        self::assertGreaterThanOrEqual($start->getTimestamp(), $fetchedMember->getUpdatedAt()->getTimestamp());
        self::assertNull($fetchedMember->getNick());
        $updatedAtBefore = $fetchedMember->getUpdatedAt();

        $member->setNick('Updated Nick');
        $repo->createOrUpdateMember($member);
        $fetchedMember = $repo->getMemberById(1);
        self::assertNotNull($fetchedMember);
        self::assertSame('Updated Nick', $fetchedMember->getNick());
        self::assertGreaterThanOrEqual($updatedAtBefore->getTimestamp(), $fetchedMember->getUpdatedAt()->getTimestamp());
    }
}
