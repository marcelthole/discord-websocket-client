<?php

namespace DiscordWebsocketClientTest\Functional;

use DiscordWebsocketClient\DiscordConfig;
use DiscordWebsocketClient\DiscordGateway;
use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\Queue\MessageListener;
use Exception;
use PHPUnit\Framework\TestCase;
use Psr\Log\Test\TestLogger;
use Ratchet\ConnectionInterface;
use Ratchet\MessageComponentInterface;
use React\EventLoop\LoopInterface;
use React\EventLoop\TimerInterface;
use DiscordWebsocketClientTest\Unit\MockFactory;

class DiscordGatewayTest extends TestCase
{

    public function testConnect() : void
    {
        $logger = new TestLogger();
        $loop = \React\EventLoop\Factory::create();
        $this->startWebSocketServer($loop);

        $config = new DiscordConfig(
            'Test Token',
            MockFactory::createNoopStorageAdapter(),
            $logger
        );
        $config->setWebsocketUrl('ws://127.0.0.1:9337');
        $gateway = new DiscordGateway(
            $loop,
            $config,
            new MessageListener(),
            null
        );
        $gateway->listen();

        $loop->addTimer(1.1, function (TimerInterface $timer) {
            TestWebsockteComponent::$sendHeartbeatAck = false;
        });
        $loop->addTimer(1.6, function (TimerInterface $timer) {
            TestWebsockteComponent::$sendHeartbeatAck = true;
        });

        $loop->addTimer(5, function (TimerInterface $timer) use ($loop) {
            $loop->stop();
        });

        $loop->run();

        self::assertCount(2, $logger->records);

    }

    private function startWebSocketServer(LoopInterface $loop): void
    {
        $component = new TestWebsockteComponent($loop);
        $webSock   = new \React\Socket\Server('127.0.0.1:9337', $loop);
        $webServer = new \Ratchet\Server\IoServer(
            new \Ratchet\Http\HttpServer(
                new \Ratchet\WebSocket\WsServer(
                    $component
                )
            ),
            $webSock,
            $loop
        );
        $loop->addTimer(2.5, function() use ($webSock) {
            $webSock->close();
            // @TODO WHY is this not working?!
        });
    }
}
