<?php

declare(strict_types=1);

namespace DiscordWebsocketClientTest\Unit\Gateway;

use DiscordWebsocketClient\Gateway\Payload;
use JsonException;
use PHPUnit\Framework\TestCase;
use function json_encode;

class PayloadTest extends TestCase
{
    /** @dataProvider createFromResponseWithExceptionDataProvider */
    public function testCreateFromResponseWithException(string $payload): void
    {
        self::expectException(JsonException::class);
        Payload::createFromResponse($payload);
    }


    /**
     * @return array<string, mixed[]>
     */
    public function createFromResponseWithExceptionDataProvider(): array
    {
        return [
            'empty string'   => [''],
            'empty object'   => ['{}'],
            'empty list'     => ['[]'],
            'invalid json'   => ['invalid'],
            'partial object' => ['{"op": 10}'],
        ];
    }

    /** @dataProvider createFromResponseWithValidPayloadDataProvider */
    public function testCreateFromResponseWithValidPayload(string $payload, array $expected): void
    {
        $payload = Payload::createFromResponse($payload);
        self::assertEquals($expected, $payload->jsonSerialize());
        self::assertEquals($expected, [
            'op' => $payload->getOperationCode(),
            'd'  => $payload->getData(),
            's'  => $payload->getSequenceNumber(),
            't'  => $payload->getEventName(),
        ]);
    }

    public function createFromResponseWithValidPayloadDataProvider()
    {
        return [
            'object with null data'                 => [
                '{"op": 10, "d": null, "s": null, "t": null}',
                [
                    'op' => 10,
                    'd'  => null,
                    's'  => null,
                    't'  => null,
                ],
            ],
            'object with filled sequence and event' => [
                '{"op": 10, "d": null, "s": 123345, "t": "event name"}',
                [
                    'op' => 10,
                    'd'  => null,
                    's'  => 123345,
                    't'  => 'event name',
                ],
            ],
            'object with empty data'                => [
                '{"op": 10, "d": {}, "s": null, "t": null}',
                [
                    'op' => 10,
                    'd'  => new \stdClass(),
                    's'  => null,
                    't'  => null,
                ],
            ],
            'object with filled data'               => [
                '{"op": 10, "d": {"foo": "bar", "baz": 1.5}, "s": null, "t": null}',
                [
                    'op' => 10,
                    'd'  => (object)[
                        'foo' => 'bar',
                        'baz' => 1.5,
                    ],
                    's'  => null,
                    't'  => null,
                ],
            ],
        ];
    }
}
