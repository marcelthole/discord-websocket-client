<?php

namespace DiscordWebsocketClientTest\Unit\Gateway\Response\DTO;

use DiscordWebsocketClient\Gateway\Response\DTO\Author;
use PHPUnit\Framework\TestCase;

class AuthorTest extends TestCase
{

    /**
     * @dataProvider createFromResponseDataProvider
     */
    public function testCreateFromResponse(\stdClass $input, Author $expected): void
    {
        $author = Author::createFromResponse($input);
        self::assertEquals($expected, $author);
    }

    public function createFromResponseDataProvider(): array
    {
        return [
            [
                json_decode('{"username": "Unit Test","id": "123456789012345678","discriminator": "4494","avatar": null}'),
                new Author('Unit Test', '123456789012345678', false),
            ],
            [
                json_decode('{"username":"Discord Bot","id":"223456789012345678","discriminator":"6119","bot":true,"avatar":null}'),
                new Author('Discord Bot', '223456789012345678', true),
            ],
        ];
    }
}
