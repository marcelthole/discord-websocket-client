<?php

declare(strict_types=1);

namespace DiscordWebsocketClientTest\Unit\Gateway;

use DiscordWebsocketClient\Gateway\HeartbeatRequest;
use PHPUnit\Framework\TestCase;

class HeartbeatTest extends TestCase
{
    /** @dataProvider createAndSerializeDataProvider */
    public function testCreateAndSerialize(
        ?int $sequenceNumber,
        array $expected
    ) : void {
        $sut = new HeartbeatRequest($sequenceNumber);
        self::assertEquals($expected, $sut->jsonSerialize());
    }

    public function createAndSerializeDataProvider() : array
    {
        return [
            'null'     => [
                null,
                [
                    'op' => 1,
                    'd'  => new \stdClass(),
                    's'  => null,
                    't'  => null,
                ],
            ],
            'negative' => [
                -1,
                [
                    'op' => 1,
                    'd'  => new \stdClass(),
                    's'  => -1,
                    't'  => null,
                ],
            ],
            'zero'     => [
                0,
                [
                    'op' => 1,
                    'd'  => new \stdClass(),
                    's'  => 0,
                    't'  => null,
                ],
            ],
            'one'      => [
                1,
                [
                    'op' => 1,
                    'd'  => new \stdClass(),
                    's'  => 1,
                    't'  => null,
                ],
            ],
        ];
    }
}
