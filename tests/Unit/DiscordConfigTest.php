<?php

namespace DiscordWebsocketClientTest\Unit;

use DiscordWebsocketClient\DiscordConfig;
use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\MessageHandler\MessageHandler;
use DiscordWebsocketClient\Queue\MessageListener;
use PHPUnit\Framework\TestCase;
use Psr\Log\LoggerInterface;
use Psr\Log\Test\TestLogger;
use Ratchet\Client\WebSocket;
use React\EventLoop\LoopInterface;

class DiscordConfigTest extends TestCase
{
    /** @var DiscordConfig */
    private $sut;

    protected function setUp(): void
    {
        $this->sut = new DiscordConfig(
            'Test Token',
            MockFactory::createNoopStorageAdapter(),
            null
        );
    }

    public function testGetWebsocketUrl(): void
    {
        self::assertNotEmpty($this->sut->getWebsocketUrl());
        $random = bin2hex(random_bytes(4));
        $this->sut->setWebsocketUrl($random);
        self::assertSame($random, $this->sut->getWebsocketUrl());
    }

    public function testGetLogger(): void
    {
        self::assertInstanceOf(LoggerInterface::class, $this->sut->getLogger());
        $logger = new TestLogger();
        $this->sut->setLogger($logger);
        self::assertSame($logger, $this->sut->getLogger());
    }

    public function testGetToken(): void
    {
        self::assertNotEmpty($this->sut->getToken());
    }
}
