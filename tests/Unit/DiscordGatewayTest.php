<?php

namespace DiscordWebsocketClientTest\Unit;

use DiscordWebsocketClient\DiscordConfig;
use DiscordWebsocketClient\DiscordGateway;
use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\MessageHandler\MessageHandler;
use DiscordWebsocketClient\Queue\MessageListener;
use DiscordWebsocketClient\Storage\StorageAdapter;
use DiscordWebsocketClient\Types\WebSocketEventType;
use PHPUnit\Framework\TestCase;
use Psr\Log\Test\TestLogger;
use Ratchet\Client\Connector;
use Ratchet\Client\WebSocket;
use Ratchet\RFC6455\Messaging\Frame;
use Ratchet\RFC6455\Messaging\Message;
use React\EventLoop\LoopInterface;
use React\EventLoop\TimerInterface;
use React\Promise\Deferred;
use React\Promise\Promise;
use React\Promise\RejectedPromise;
use React\Socket\ConnectorInterface;

class DiscordGatewayTest extends TestCase
{
    public function testListenFailed(): void
    {
        $logger = new TestLogger();
        $config = MockFactory::createConfigStub($logger);

        $noopLoop = $this->createMock(LoopInterface::class);
        $noopLoop->expects(self::once())->method('stop');

        $sut = new DiscordGateway(
            $noopLoop,
            $config,
            new MessageListener(),
            MockFactory::createConnectorStub(function () {
                return new RejectedPromise(new \Exception('Unit Test', 12345));
            })
        );


        self::assertEmpty($logger->records);
        $sut->listen();
        self::assertTrue($logger->hasCriticalRecords());
    }

    public function testListenSuccess(): void
    {

        $websocket = new class extends WebSocket
        {
            public function __construct()
            {
            }
        };

        $logger = new TestLogger();
        $config = MockFactory::createConfigStub($logger);

        $noopLoop = $this->createMock(LoopInterface::class);
        $noopLoop->expects(self::never())->method('stop');

        $messageListener = new MessageListener();

        $sut = new DiscordGateway(
            $noopLoop,
            $config,
            $messageListener,
            MockFactory::createConnectorStub(function () use ($websocket) {
                $deffer = new Deferred();
                $deffer->resolve($websocket);
                return $deffer->promise();
            })
        );

        $messageHandler = $this->createMock(MessageHandler::class);
        $messageHandler->expects(self::once())->method('handleMessage');
        $messageListener->add($messageHandler);

        self::assertEmpty($logger->records);

        $sut->listen();

        $message = MockFactory::createPayloadMessage(
            new Payload(Payload::OP_HELLO, null, null, null)
        );
        $websocket->emit(WebSocketEventType::TYPE_MESSAGE, [$message]);

        self::assertTrue($logger->hasDebugThatContains('Connected to websocket'));
        self::assertStringContainsString(
            'Received message: {"op":10,"d":null,"s":null,"t":null}',
            $logger->recordsByLevel['debug'][1]['message']
        );
        self::assertCount(2, $logger->records);
    }
}
