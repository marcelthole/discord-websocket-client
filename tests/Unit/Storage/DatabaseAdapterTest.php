<?php

namespace DiscordWebsocketClientTest\Unit\Storage;

use DiscordWebsocketClient\Storage\DatabaseAdapter;
use PHPUnit\Framework\TestCase;

class DatabaseAdapterTest extends TestCase
{

    public function testQuery(): void
    {
        $pdo = new \PDO('sqlite::memory:');
        $adapter = new DatabaseAdapter($pdo);
        $stmt = $adapter->query('SELECT 1, :id', ['id' => 5]);
        self::assertSame('SELECT 1, :id', $stmt->queryString);
        self::assertSame(['1', '5'], $stmt->fetch(\PDO::FETCH_NUM));
    }
}
