<?php

namespace DiscordWebsocketClientTest\Unit\Storage\Repository;

use DiscordWebsocketClient\Storage\Entity\Member;
use DiscordWebsocketClient\Storage\Repository\MemberRepository;
use DiscordWebsocketClient\Storage\StorageAdapter;
use PHPUnit\Framework\TestCase;
use DiscordWebsocketClientTest\Unit\MockFactory;

class MemberRepositoryTest extends TestCase
{

    public function testGetMemberByIdWithoutResult(): void
    {
        $stmt = $this->createMock(\PDOStatement::class);
        $stmt->method('fetch')->willReturn(
            null,
            (object) [
                'id' => 1,
                'username' => 'Unittest',
                'nick' => null,
                'updatedAt' => '01.02.2003 14:56:17'
            ],
            (object) [
                'id' => 1,
                'username' => 'Unittest',
                'nick' => 'nick name',
                'updatedAt' => '01.02.2003 14:56:17'
            ]
        );
        $adapter = $this->createMock(StorageAdapter::class);
        $adapter->method('query')->willReturn($stmt);
        $repository = new MemberRepository($adapter);

        $memberCall1 = $repository->getMemberById(1);
        self::assertNull($memberCall1);
        $memberCall2 = $repository->getMemberById(1);
        self::assertNotNull($memberCall2);
        self::assertSame(1, $memberCall2->getId());
        self::assertSame('Unittest', $memberCall2->getUsername());
        self::assertNull($memberCall2->getNick());
        self::assertSame('01.02.2003 14:56:17', $memberCall2->getUpdatedAt()->format('d.m.Y H:i:s'));

        $memberCall3 = $repository->getMemberById(1);
        self::assertNotNull($memberCall3);
        self::assertSame('nick name', $memberCall3->getNick());
    }
}
