<?php

namespace DiscordWebsocketClientTest\Unit\Queue;

use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\MessageHandler\MessageHandler;
use DiscordWebsocketClient\Queue\MessageListener;
use PHPUnit\Framework\TestCase;
use Ratchet\Client\WebSocket;
use React\EventLoop\LoopInterface;

class MessageListenerTest extends TestCase
{
    /** @var MessageListener */
    private $sut;

    protected function setUp(): void
    {
        $this->sut = new MessageListener();
    }

    public function testGetOnMessageListener(): void
    {
        $this->sut->add(new class implements MessageHandler {
            public function handleMessage(LoopInterface $loop, WebSocket $webSocket, Payload $payload): void
            {
                // do nothing
            }

            public function handleClose(LoopInterface $loop, WebSocket $webSocket): void
            {
                // do nothing
            }
        });

        self::assertCount(1, $this->sut->getRegisteredHandlers());
    }
}
