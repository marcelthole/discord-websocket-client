<?php


namespace DiscordWebsocketClientTest\Unit;


use DiscordWebsocketClient\DiscordConfig;
use DiscordWebsocketClient\Gateway\Payload;
use DiscordWebsocketClient\Storage\StorageAdapter;
use Psr\Log\LoggerInterface;
use Psr\Log\NullLogger;
use Ratchet\Client\Connector;
use Ratchet\MessageInterface;
use Ratchet\RFC6455\Messaging\Frame;
use Ratchet\RFC6455\Messaging\Message;

class MockFactory
{
    public static function createConnectorStub(callable $invokable) : Connector
    {
        return new class($invokable) extends Connector
        {
            /** @var callable */
            private $invokable;

            public function __construct(callable $invokable)
            {
                $this->invokable = $invokable;
            }

            public function __invoke($url, array $subProtocols = [], array $headers = [])
            {
                return ($this->invokable)();
            }
        };
    }

    public static function createConfigStub(?LoggerInterface $logger) : DiscordConfig
    {
        $config = new DiscordConfig(
            'Unit Test Token',
            self::createNoopStorageAdapter(),
            $logger ?: new NullLogger()
        );
        return $config;
    }

    public static function createNoopStorageAdapter() : StorageAdapter
    {
        return new class implements StorageAdapter
        {
            public function query(string $sql, array $args = []): \PDOStatement
            {
                return new \PDOStatement();
            }
        };
    }

    public static function createPayloadMessage(Payload $payload): Message
    {
        $message = new Message();
        $message->addFrame(new Frame(json_encode($payload) ?: null));
        return $message;
    }
}
