create table Members
(
	id integer constraint Members_pk primary key,
	username varchar(255) not null,
	nick varchar(255),
	updatedAt DATETIME default CURRENT_TIMESTAMP
);
create unique index Members_id_uindex on Members (id);