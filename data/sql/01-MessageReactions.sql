create table MessageReactions
(
	messageId int not null,
	userId int not null
		constraint MessageReactions_Members_id_fk
			references Members,
	reaction VARCHAR(64) not null,
	createdAt DATETIME default CURRENT_TIMESTAMP
);

create index MessageReactions_messageId_index
	on MessageReactions (messageId);

